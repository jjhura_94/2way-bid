function initConnectRabbit(routingKey){
        // var has_had_focus = false;
        // var pipe = function(el_name, send) {
        //     var div  = $(el_name + ' div');
        //     var inp  = $(el_name + ' input');
        //     var form = $(el_name + ' form');

        //     var print = function(m, p) {
        //         p = (p === undefined) ? '' : JSON.stringify(p);
        //         div.append($("<code>").text(m + ' ' + p));
        //         div.scrollTop(div.scrollTop() + 10000);
        //     };

        //     if (send) {
        //         form.submit(function() {
        //             send(inp.val());
        //             inp.val('');
        //             return false;
        //         });
        //     }
        //     return print;
        // };

        // var print_first = pipe('#first', function(data) {
        //     message = new Paho.MQTT.Message(data);
        //     message.destinationName = "test";
        //     debug("SEND ON " + message.destinationName + " PAYLOAD " + data);
        //     client.send(message);
        // });

        // var debug = pipe('#second');

       // var wsbroker = location.hostname;  //mqtt websocket enabled broker
	   var wsbroker = "localhost"
        var wsport = 15675; // port for above

        var client = new Paho.MQTT.Client(wsbroker, wsport, "/ws",
            "myclientid" + new Date().getTime());

        client.onConnectionLost = function (responseObject) {
            console.log("CONNECTION LOST - " + responseObject.errorMessage);
        };

        client.onMessageArrived = function (message) {
            // console.log("RECEIVE ON " + message.destinationName + " PAYLOAD " + message.payloadString);
            console.log(message.payloadString)
            localStorage.setItem("data-bid",message.payloadString)
            var res = JSON.parse(message.payloadString)
            if(res?.type == "BID_DETAIL" && res?.data?.length > 0){
                let div = ''
                res?.data?.forEach(item => {
                    div = div + '<tr class="mat-row ng-star-inserted"> <td class="mat-cell"><i class="fas fa-trophy ng-star-inserted"></i> <div class="userName"> <div class="bidUserName">'+
                    item?.ownerEmail
                    +'</div> </div> </td> <td class="mat-cell">'+
                    item?.price
                    +'</td> <td class="mat-cell">1</td> <td class="mat-cell">'+
                    new Date(item?.time)
                    +'</td> </tr>'
                });
                console.log(123123,div)
                $('#his-body').html(div);
            }
        };

        var options = {
            timeout: 3,
            keepAliveInterval: 30,
            onSuccess: function () {
                console.log("CONNECTION SUCCESS");
                client.subscribe(routingKey, {qos: 1});
            },
            onFailure: function (message) {
                console.log("CONNECTION FAILURE - " + message.errorMessage);
            }
        };

        if (location.protocol == "https:") {
            options.useSSL = true;
        }

        console.log("CONNECT TO " + wsbroker + ":" + wsport);
        client.connect(options);

        // $('#first input').focus(function() {
        //     if (!has_had_focus) {
        //         has_had_focus = true;
        //         $(this).val("");
        //     }
        // });
}

function fillDataToTable(){

}