import { ProductComponent } from './pages/product/product.component';
import { AppLandingComponent } from './pages/app-landing/app-landing.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppIndexComponent} from "./pages/app-index/app-index.component";
import {FeedComponent} from "./pages/feed/feed.component";

const routes: Routes = [
  {
    path: '',
    component : AppLandingComponent
  },
  {
    path: 'my-bid',
    component : AppIndexComponent
  },
  {
    path: 'feed',
    component : FeedComponent
  },
  {
    path: ':id',
    component : ProductComponent
  },
 
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
