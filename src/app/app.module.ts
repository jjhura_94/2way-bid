import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { AppLandingComponent } from './pages/app-landing/app-landing.component';
import { BidBuySmileComponent } from './pages/bid-buy-smile/bid-buy-smile.component';
import { FeedItemComponent } from './pages/app-landing/feed-item/feed-item.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductComponent } from './pages/product/product.component';
import { ProductGalleryComponent } from './pages/product/product-gallery/product-gallery.component';
import { ProductPromisComponent } from './pages/product/product-promis/product-promis.component';
import { ProductDetailsComponent } from './pages/product/product-details/product-details.component';
import { BiddingHistoryComponent } from './pages/product/bidding-history/bidding-history.component';
import { CountDownComponent } from './pages/product/count-down/count-down.component';
import { ProductLayoutComponent } from './pages/product/product-layout/product-layout.component';
import { AppIndexComponent } from './pages/app-index/app-index.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { httpInterceptorProviders } from './core/interceptor';
import { FeedComponent } from './pages/feed/feed.component';
import { CampaignsComponent } from './components/campaigns/campaigns.component';
import { EndlessFeedComponent } from './pages/feed/endless-feed/endless-feed.component';
import { GridTileItemComponent } from './pages/feed/grid-tile-item/grid-tile-item.component';
import { FeedNavComponent } from './pages/feed/feed-nav/feed-nav.component';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    declarations: [
        AppComponent,
        AppLandingComponent,
        BidBuySmileComponent,
        FeedItemComponent,
        LoginComponent,
        RegisterComponent,
        ProductComponent,
        ProductGalleryComponent,
        ProductPromisComponent,
        ProductDetailsComponent,
        BiddingHistoryComponent,
        CountDownComponent,
        ProductLayoutComponent,
        AppIndexComponent,
        FeedComponent,
        CampaignsComponent,
        EndlessFeedComponent,
        GridTileItemComponent,
        FeedNavComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        SharedModule,
        BrowserAnimationsModule,
        HttpClientModule,
        // ServiceWorkerModule.register('ngsw-worker.js', { enabled: false }),
        NgxWebstorageModule.forRoot({ prefix: 'jhi', separator: '-', caseSensitive: true }),
        ToastrModule.forRoot(),

    ],
    providers: [
      httpInterceptorProviders,
    ],
  exports: [
    CountDownComponent
  ],
    bootstrap: [AppComponent]
})
export class AppModule { }
