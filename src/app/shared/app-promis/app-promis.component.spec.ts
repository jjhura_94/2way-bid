import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPromisComponent } from './app-promis.component';

describe('AppPromisComponent', () => {
  let component: AppPromisComponent;
  let fixture: ComponentFixture<AppPromisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppPromisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPromisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
