import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppLandingHeaderComponent } from './app-landing-header.component';

describe('AppLandingHeaderComponent', () => {
  let component: AppLandingHeaderComponent;
  let fixture: ComponentFixture<AppLandingHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppLandingHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppLandingHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
