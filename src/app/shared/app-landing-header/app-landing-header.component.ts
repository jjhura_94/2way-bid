import { Component, Input, OnInit } from '@angular/core';
import { Account } from 'src/app/core/auth/account.model';
import { SubjectService } from '../subject.service';

@Component({
  selector: 'app-landing-header',
  templateUrl: './app-landing-header.component.html',
  styleUrls: ['./app-landing-header.component.scss']
})
export class AppLandingHeaderComponent implements OnInit {

  constructor(private subjectService: SubjectService) { }

  @Input()
  account : any
  
  

  ngOnInit(): void {
    this.subjectService.getMessage().subscribe(data => {
      this.getAccount();
    })
    this.getAccount();
   
  }
  getAccount(){
    let accStorage = localStorage.getItem("account");
    if(accStorage){
      this.account = JSON.parse(accStorage);
    }
  }

}
