import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAuctionsButtonComponent } from './my-auctions-button.component';

describe('MyAuctionsButtonComponent', () => {
  let component: MyAuctionsButtonComponent;
  let fixture: ComponentFixture<MyAuctionsButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyAuctionsButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAuctionsButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
