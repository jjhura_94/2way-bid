import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-auctions-button',
  templateUrl: './my-auctions-button.component.html',
  styleUrls: ['./my-auctions-button.component.scss']
})
export class MyAuctionsButtonComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  ahihi(){
    this.router.navigate(['/my-bid']);
  }

}
