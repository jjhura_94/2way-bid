import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallAuctionListItemComponent } from './small-auction-list-item.component';

describe('SmallAuctionListItemComponent', () => {
  let component: SmallAuctionListItemComponent;
  let fixture: ComponentFixture<SmallAuctionListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmallAuctionListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallAuctionListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
