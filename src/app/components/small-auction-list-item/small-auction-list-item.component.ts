import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-small-auction-list-item',
  templateUrl: './small-auction-list-item.component.html',
  styleUrls: ['./small-auction-list-item.component.scss']
})
export class SmallAuctionListItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
