import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BidBuySmileComponent } from './bid-buy-smile.component';

describe('BidBuySmileComponent', () => {
  let component: BidBuySmileComponent;
  let fixture: ComponentFixture<BidBuySmileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BidBuySmileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BidBuySmileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
