import {Component, OnInit} from '@angular/core';
import { BidService } from '../../product/product-details/service/bid.service';

@Component({
  selector: 'app-endless-feed',
  templateUrl: './endless-feed.component.html',
  styleUrls: ['./endless-feed.component.scss']
})
export class EndlessFeedComponent implements OnInit {

  items: any = [];

  constructor(
    protected bidService: BidService
  ) {
  }

  ngOnInit(): void {

    this.bidService.query().subscribe(
      res => {
        if(res.body){
          console.log("data 123",res.body);
          res.body.forEach(item => {
            const newItem = {
              itemNumber: item.id,
              href: "/"+item.id,
              img: "https://cdn.chilindo.com//XML/Gfx/22-158/22-158presell-1_250.jpg",
              title: item.product?.name,
              subTitle: item.product?.description?.substr(0,20)+"...",
              age: 50,
              countDownTime: "10:15",
              price: item.product?.price
            };
            this.items.push(newItem);
          });
        }
      }
    )

    // for (let i = 0; i < 1000; i++) {
    //   const newItem = {
    //     itemNumber: i,
    //     href: "/product",
    //     img: "https://cdn.chilindo.com//XML/Gfx/22-158/22-158presell-1_250.jpg",
    //     title: "Title " + i,
    //     subTitle: "Here is sub title " + i,
    //     age: 50,
    //     countDownTime: "10:15",
    //     price: Math.floor(Math.random() * 100) * 1000
    //   };
    //   this.items.push(newItem);
    // }


  }

}
