import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EndlessFeedComponent } from './endless-feed.component';

describe('EndlessFeedComponent', () => {
  let component: EndlessFeedComponent;
  let fixture: ComponentFixture<EndlessFeedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EndlessFeedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EndlessFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
