import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-grid-tile-item',
  templateUrl: './grid-tile-item.component.html',
  styleUrls: ['./grid-tile-item.component.scss']
})
export class GridTileItemComponent implements OnInit {

  @Input()
  item: any;
  constructor() { }

  ngOnInit(): void {
    console.log(this.item);
  }

}
