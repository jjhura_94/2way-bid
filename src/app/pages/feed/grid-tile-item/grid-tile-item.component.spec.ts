import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridTileItemComponent } from './grid-tile-item.component';

describe('GridTileItemComponent', () => {
  let component: GridTileItemComponent;
  let fixture: ComponentFixture<GridTileItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridTileItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridTileItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
