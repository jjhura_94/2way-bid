import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';
import { getBidIdentifier, IBid } from '../model/bid.model';
import { ApplicationConfigService } from 'src/app/core/config/application-config.service';
import { createRequestOption } from 'src/app/core/request/request-util';
import { isPresent } from 'src/app/core/util/operators';



export type EntityResponseType = HttpResponse<IBid>;
export type EntityArrayResponseType = HttpResponse<IBid[]>;

@Injectable({ providedIn: 'root' })
export class BidService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/bids');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(bid: IBid): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bid);
    return this.http
      .post<IBid>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bid: IBid): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bid);
    return this.http
      .put<IBid>(`${this.resourceUrl}/${getBidIdentifier(bid) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(bid: IBid): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bid);
    return this.http
      .patch<IBid>(`${this.resourceUrl}/${getBidIdentifier(bid) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBid>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBid[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addBidToCollectionIfMissing(bidCollection: IBid[], ...bidsToCheck: (IBid | null | undefined)[]): IBid[] {
    const bids: IBid[] = bidsToCheck.filter(isPresent);
    if (bids.length > 0) {
      const bidCollectionIdentifiers = bidCollection.map(bidItem => getBidIdentifier(bidItem)!);
      const bidsToAdd = bids.filter(bidItem => {
        const bidIdentifier = getBidIdentifier(bidItem);
        if (bidIdentifier == null || bidCollectionIdentifiers.includes(bidIdentifier)) {
          return false;
        }
        bidCollectionIdentifiers.push(bidIdentifier);
        return true;
      });
      return [...bidsToAdd, ...bidCollection];
    }
    return bidCollection;
  }

  protected convertDateFromClient(bid: IBid): IBid {
    return Object.assign({}, bid, {
      startTime: bid.startTime?.isValid() ? bid.startTime.toJSON() : undefined,
      endTime: bid.endTime?.isValid() ? bid.endTime.toJSON() : undefined,
      createdDate: bid.createdDate?.isValid() ? bid.createdDate.toJSON() : undefined,
      updatedDate: bid.updatedDate?.isValid() ? bid.updatedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startTime = res.body.startTime ? dayjs(res.body.startTime) : undefined;
      res.body.endTime = res.body.endTime ? dayjs(res.body.endTime) : undefined;
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.updatedDate = res.body.updatedDate ? dayjs(res.body.updatedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bid: IBid) => {
        bid.startTime = bid.startTime ? dayjs(bid.startTime) : undefined;
        bid.endTime = bid.endTime ? dayjs(bid.endTime) : undefined;
        bid.createdDate = bid.createdDate ? dayjs(bid.createdDate) : undefined;
        bid.updatedDate = bid.updatedDate ? dayjs(bid.updatedDate) : undefined;
      });
    }
    return res;
  }
}
