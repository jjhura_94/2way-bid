import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';





import { isPresent } from 'src/app/core/util/operators';
import { ApplicationConfigService } from 'src/app/core/config/application-config.service';
import { getBidDesIdentifier, IBidDes } from '../model/bid-des.model';
import { createRequestOption } from 'src/app/core/request/request-util';

export type EntityResponseType = HttpResponse<IBidDes>;
export type EntityArrayResponseType = HttpResponse<IBidDes[]>;

@Injectable({ providedIn: 'root' })
export class BidDesService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/bid-des');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(bidDes: IBidDes): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bidDes);
    return this.http
      .post<IBidDes>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bidDes: IBidDes): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bidDes);
    return this.http
      .put<IBidDes>(`${this.resourceUrl}/${getBidDesIdentifier(bidDes) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(bidDes: IBidDes): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bidDes);
    return this.http
      .patch<IBidDes>(`${this.resourceUrl}/${getBidDesIdentifier(bidDes) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBidDes>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBidDes[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getByBidId(bidId : number): Observable<EntityArrayResponseType> {
    return this.http
      .get<IBidDes[]>(this.resourceUrl+"/get-by-bid/"+bidId, { params: {}, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addBidDesToCollectionIfMissing(bidDesCollection: IBidDes[], ...bidDesToCheck: (IBidDes | null | undefined)[]): IBidDes[] {
    const bidDes: IBidDes[] = bidDesToCheck.filter(isPresent);
    if (bidDes.length > 0) {
      const bidDesCollectionIdentifiers = bidDesCollection.map(bidDesItem => getBidDesIdentifier(bidDesItem)!);
      const bidDesToAdd = bidDes.filter(bidDesItem => {
        const bidDesIdentifier = getBidDesIdentifier(bidDesItem);
        if (bidDesIdentifier == null || bidDesCollectionIdentifiers.includes(bidDesIdentifier)) {
          return false;
        }
        bidDesCollectionIdentifiers.push(bidDesIdentifier);
        return true;
      });
      return [...bidDesToAdd, ...bidDesCollection];
    }
    return bidDesCollection;
  }

  protected convertDateFromClient(bidDes: IBidDes): IBidDes {
    return Object.assign({}, bidDes, {
      time: bidDes.time?.isValid() ? bidDes.time.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.time = res.body.time ? dayjs(res.body.time) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bidDes: IBidDes) => {
        bidDes.time = bidDes.time ? dayjs(bidDes.time) : undefined;
      });
    }
    return res;
  }
}
