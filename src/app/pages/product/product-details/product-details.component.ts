import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Bid } from './model/bid.model';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  @Input("detail")
  bid: Bid = new Bid();

  constructor(protected activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
