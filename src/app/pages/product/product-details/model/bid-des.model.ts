import * as dayjs from 'dayjs';
import { IBid } from './bid.model';



export interface IBidDes {
  id?: number;
  time?: dayjs.Dayjs | null;
  price?: number | null;
  bid?: IBid | null;
  userBid?: any | null;
  userAction? : any | null
}

export class BidDes implements IBidDes {
  constructor(
    public id?: number,
    public time?: dayjs.Dayjs | null,
    public price?:number | null,
    public bid?: IBid | null,
    public userBid?: any | null,
    public userAction? : any | null
  ) {}
}

export function getBidDesIdentifier(bidDes: IBidDes): number | undefined {
  return bidDes.id;
}
