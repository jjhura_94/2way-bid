import * as dayjs from 'dayjs';
import { IBidDes } from './bid-des.model';
import { IProduct } from './product.model';

export interface IBid {
  id?: number;
  startPrice?: number | null;
  startTime?: dayjs.Dayjs | null;
  endTime?: dayjs.Dayjs | null;
  status?: string | null;
  createdBy?: string | null;
  createdDate?: dayjs.Dayjs | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  bidDes?: IBidDes[] | null;
  product?: IProduct | null;
}

export class Bid implements IBid {
  constructor(
    public id?: number,
    public startPrice?: number | null,
    public startTime?: dayjs.Dayjs | null,
    public endTime?: dayjs.Dayjs | null,
    public status?: string | null,
    public createdBy?: string | null,
    public createdDate?: dayjs.Dayjs | null,
    public updatedDate?: dayjs.Dayjs | null,
    public updatedBy?: string | null,
    public bidDes?: IBidDes[] | null,
    public product?: IProduct | null
  ) {}
}

export function getBidIdentifier(bid: IBid): number | undefined {
  return bid.id;
}
