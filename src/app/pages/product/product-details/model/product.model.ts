import * as dayjs from 'dayjs';

export interface IProduct {
  id?: number;
  name?: string | null;
  description?: string | null;
  price?: number | null;
  extraInfo?: string | null;
  createdBy?: string | null;
  createdDate?: dayjs.Dayjs | null;
  updatedDate?: dayjs.Dayjs | null;
  updatedBy?: string | null;
  productImages?: any[] | null;
  bids?: any[] | null;
  owner?: any | null;
  category?: any | null;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string | null,
    public description?: string | null,
    public price?: number | null,
    public extraInfo?: string | null,
    public createdBy?: string | null,
    public createdDate?: dayjs.Dayjs | null,
    public updatedDate?: dayjs.Dayjs | null,
    public updatedBy?: string | null,
    public productImages?: any[] | null,
    public bids?: any[] | null,
    public owner?: any | null,
    public category?: any | null
  ) {}
}

export function getProductIdentifier(product: IProduct): number | undefined {
  return product.id;
}
