import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Bid } from './product-details/model/bid.model';
import { BidDesService } from './product-details/service/bid-des.service';
import { BidService } from './product-details/service/bid.service';

declare function initConnectRabbit(routingKey:any) : any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

 
  bid : Bid = new Bid();
  constructor(protected activatedRoute: ActivatedRoute,
              protected bidService: BidService,
              protected bidDesService: BidDesService
              ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      const id = params.get('id');
      if(id){
        var idNumber: number = +id;
        this.bidService.find(idNumber).subscribe(
          res => {
            if(res.body){
              this.bid = res.body;
              initConnectRabbit("join-bid:"+this.bid.id);
            }
          }
        )
      }
    });
  }

}
