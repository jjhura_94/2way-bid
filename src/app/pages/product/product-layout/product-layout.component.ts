import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as dayjs from 'dayjs';
import { ToastrService } from 'ngx-toastr';
import { BidDes } from '../product-details/model/bid-des.model';
import { Bid } from '../product-details/model/bid.model';
import { BidDesService } from '../product-details/service/bid-des.service';
import { BidService } from '../product-details/service/bid.service';

@Component({
  selector: 'app-product-layout',
  templateUrl: './product-layout.component.html',
  styleUrls: ['./product-layout.component.scss']
})
export class ProductLayoutComponent implements OnInit ,OnChanges{

  constructor(
    protected bidDesService: BidDesService,
    private toastr: ToastrService
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
      if(this.bid){
        this.price = this.bid?.product?.price ;
        if(this.bid?.id){
          this.bidDesService.getByBidId(this.bid?.id).subscribe(
            res => {
              if(res.body){
                this.bidDes = res.body;
              }
              
            }
          )
        }
        
      }
  }

  @Input("detail")
  bid: Bid = new Bid();

  price : any = 0;

  bidDes: BidDes[] = [];


  ngOnInit(): void {
    
  }

  auction(){
    if(this.bid?.product?.price && this.price < this.bid?.product?.price){
      this.showError("Giá bạn đưa gia nhỏ hơn giá yêu cầu!!!")
    }
    else if(this.bid?.product?.price){
      let bidDes = new BidDes();
      bidDes.bid = this.bid;
      bidDes.price = this.price;
      bidDes.time = dayjs().startOf('day');
      this.bidDesService.create(bidDes).subscribe(
        res => {
          this.showSuccess("Đặt giá "+ this.price +" cho sản phẩm thành công !!")
        },
        err => {
          this.showError("có lỗi xảy ra vui lòng thử lại")
        }
      )
    }
  }

  showSuccess(message: any) {
    this.toastr.success(message, 'Thông báo');
  }

  showError(message: any) {
    this.toastr.error(message, 'Thông báo');
  }

}
