import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-count-down',
  templateUrl: './count-down.component.html',
  styleUrls: ['./count-down.component.scss']
})
export class CountDownComponent implements OnInit {

  @Input()
  hours = 0;

  @Input()
  minutes = 0;

  @Input()
  seconds = 60;

  isRunning = true;

  constructor() {

  }

  varName() {

    if (this.seconds == 0 && this.minutes == 0 && this.minutes == 0) {
      this.isRunning = false;
    } else {
      if (this.seconds == 0) {
        if (this.minutes == 0) {
          this.seconds = 60
          if (this.hours == 0) {
            this.minutes = 59;
          } else
            this.hours--;
        } else {
          this.minutes--;
        }
      } else {
        this.seconds--;
      }
    }
  };

  ngOnInit(): void {
    // if (!this.hours) this.hours = 0;
    // if (!this.minutes) this.minutes = 0;
    // if (!this.seconds) this.seconds = 0;

    var intervalId = setInterval(() => {
      if (this.seconds == 0 && this.minutes == 0 && this.minutes == 0) {
        this.isRunning = false;
      }
      // else {
      if (this.seconds == 0) {
        if (this.minutes == 0) {
          if (this.hours != 0){
            this.hours--;
            this.minutes = 59;
          }
        } else {
          this.seconds = 59;
          this.minutes --;
        }
      } else {
        this.seconds--;
      }
      // }
    }, 1000);
  }
}

