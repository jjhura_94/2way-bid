import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductPromisComponent } from './product-promis.component';

describe('ProductPromisComponent', () => {
  let component: ProductPromisComponent;
  let fixture: ComponentFixture<ProductPromisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductPromisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPromisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
