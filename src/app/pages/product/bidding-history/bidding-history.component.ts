import { Component, Input, OnInit } from '@angular/core';
import { BidDes } from '../product-details/model/bid-des.model';

@Component({
  selector: 'app-bidding-history',
  templateUrl: './bidding-history.component.html',
  styleUrls: ['./bidding-history.component.scss']
})
export class BiddingHistoryComponent implements OnInit {

  constructor() { }

  @Input()
  bidDes : BidDes[] = [];

  ngOnInit(): void {
    
  }

}
